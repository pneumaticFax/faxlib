//
// Created by Jon_Austin on 2/28/2017.
//

#ifndef FAXLIB_FAXQUEUE_H
#define FAXLIB_FAXQUEUE_H

#include "../FaxSList/FaxSList.h"

template <class T>
class FaxQueue : public FaxSList<T>{

public:
    //function pushes data provided to head of queue, returns whole queue
    FaxQueue<T>* push(T);

    //function pops data provided from tail of queue
    FaxQueue<T> * pop();

    //function returns data provided from element of queue
    T peek(FaxNode<T> *);

};



template <class T>
FaxQueue<T>* FaxQueue<T>::push(T data){
    return (FaxQueue*) this->insertNode(data);
}

template <class T>
FaxQueue<T> * FaxQueue<T>::pop(){
    return (FaxQueue<T>*) this->removeNode(this->tail);
}

template <class T>
T FaxQueue<T>::peek(FaxNode<T> *element){
    return element->data;
}
#endif //FAXLIB_FAXQUEUE_H
