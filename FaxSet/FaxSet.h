//
// Created by Jon_Austin on 2/22/2017.
//

#ifndef FAXLIB_FAXSET_H
#define FAXLIB_FAXSET_H


#include "../FaxNode/FaxNode.h"
#include "../FaxList/FaxList.h"

template <class T>
class FaxSet : FaxList<T>  {
public:
    FaxNode<T>* hasElement(T data);

    bool setInsert(T);
    bool setRemove(T);
    bool setClear();

    FaxSet<T> *getIntersection(FaxSet<T>*);
    FaxSet<T> *getUnion(FaxSet<T>*);
    FaxSet<T> *getCompliment(FaxSet<T>*);
    FaxSet<T> *getDifference(FaxSet<T>*);

    bool isSubset(FaxSet<T> *);
    bool isEmpty();

    const void printSet(){FaxList<T>::printList();}

private:

};

template <class T>
FaxNode<T>* FaxSet<T>::hasElement(T data){
    FaxNode<T> *cursor;
    for(cursor = this->getHead(); cursor != nullptr; cursor = cursor->next) {
        if (cursor->data == data) {
            return cursor;
        }
    }
    return nullptr;
}

template <class T>
bool FaxSet<T>::setInsert(T data){
    if(this->getSize() == 0){
        this->insertNodeHead(data);
        return true;
    }else if(!this->hasElement(data)){
        this->insertNodeHead(data);
        return true;
    }
    return false;
}

template<class T>
bool FaxSet<T>::setRemove(T data){
    if(this->getSize() < 1){
        return false;
    }
    FaxNode<T> *cursor = this->hasElement(data);
    if(cursor != nullptr) {
        if(cursor == this->getHead()){
            this->removeNodeHead();
        }else if( cursor == this->getTail()){
            this->removeNodeTail();
        }else{
            this->removeNodeNext(cursor->prev);
        }
    }
    return true;
}

template<class T>
bool FaxSet<T>::setClear() {
    FaxNode<T> *cursor;
    for(cursor = this->getHead(); cursor != nullptr; cursor = cursor->next){
        this->removeNodeHead();
    }
}


template <class T>
FaxSet<T>* FaxSet<T>::getIntersection(FaxSet<T>* set2) {
    FaxSet<T> *intersection = new FaxSet<T>();
    if(this->getSize() == 0 || set2->getSize() == 0){
        return intersection;
    }
    FaxNode<T> *cursor;
    for(cursor = this->getHead(); cursor != nullptr; cursor = cursor->next){
        if(set2->hasElement(cursor->data)){
            intersection->insertNodeHead(cursor->data);
        }
    }
    return intersection;
}

template <class T>
FaxSet<T>* FaxSet<T>::getUnion(FaxSet<T> *set2){
    FaxSet<T> *setUnion = new FaxSet<T>();
    FaxNode<T> *cursor;
    if(this->getSize() ==0 && set2->getSize() == 0){
        return setUnion;
    }
    for(cursor = this->getHead(); cursor != nullptr; cursor = cursor->next){
        setUnion->setInsert(cursor->data);
    }
    for(cursor = set2->getHead(); cursor != nullptr; cursor = cursor->next){
        setUnion->setInsert(cursor->data);
    }
    return setUnion;
}


template <class T>
FaxSet<T>* FaxSet<T>::getDifference(FaxSet<T>* set2) {
    FaxSet<T> *difference = new FaxSet<T>();
    if(this->getSize() == 0 || set2->getSize() == 0){
        return difference;
    }
    FaxNode<T> *cursor;
    for(cursor = this->getHead(); cursor != nullptr; cursor = cursor->next){
        if(set2->hasElement(cursor->data) == nullptr){
            difference->insertNodeHead(cursor->data);
        }
    }
    for(cursor = set2->getHead(); cursor != nullptr; cursor = cursor->next){
        if(this->hasElement(cursor->data) == nullptr){
            difference->insertNodeHead(cursor->data);
        }
    }
    return difference;
}

template <class T>
FaxSet<T>* FaxSet<T>::getCompliment(FaxSet<T>* set2) {
    FaxSet<T> *compliment = new FaxSet<T>;
    if(set2->getSize() == 0){
        return this;
    }else{
        FaxNode<T> *cursor;
        for(cursor = this->getHead(); cursor != nullptr; cursor = cursor->next){
            if(set2->hasElement(cursor->data) == nullptr){
                compliment->setInsert(cursor->data);
            }
        }
    }
    return compliment;
}


template <class T>
bool FaxSet<T>::isSubset(FaxSet<T> *set2) {
    if(this->getSize() > set2->getSize()){return false;}
    FaxNode<T> *cursor;
    for(cursor = this->getHead(); cursor != nullptr; cursor = cursor->next){
        if(set2->hasElement(cursor->data) == nullptr){return false;}
    }
    return true;
}

template <class T>
bool FaxSet<T>::isEmpty() {
    return (this->getSize() == 0);
}

#endif //FAXLIB_FAXSET_H
