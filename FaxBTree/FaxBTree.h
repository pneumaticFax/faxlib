//
// Created by Jon_Austin on 2/24/2017.
//

#ifndef FAXLIB_FAXBTREE_H
#define FAXLIB_FAXBTREE_H

#include <algorithm>
#include <iostream>
#include <iomanip>
#include "../FaxNode/FaxTreeNode.h"
#include "../FaxQueue/FaxQueue.h"


template <class T>
class FaxBTree {
public:

    FaxTreeNode<int, T> *root;
    int size;


    FaxBTree<T>(): root(nullptr), size(0){}
    FaxBTree<T>(const FaxBTree<T>& copy): root(copy.root), size(copy.size){}
    ~FaxBTree<T>(){delete(root); root = nullptr;}

    FaxBTree<T>* insertNode(int, T);
    FaxTreeNode<int, T> * insertNode(FaxTreeNode<int, T> *, int, T);
    int getHeight(FaxTreeNode<int, T> *);
    int getBalance(FaxTreeNode<int, T> *);
    FaxTreeNode<int,T>* rotateRight(FaxTreeNode<int,T> *);
    FaxTreeNode<int,T>* rotateLeft(FaxTreeNode<int,T> *);

    const void printTree(FaxTreeNode<int,T> *node, FaxQueue<FaxTreeNode<int,T>*> *queue);
};

template <class T>
FaxTreeNode<int, T> * FaxBTree<T>::insertNode(FaxTreeNode<int, T> *node, int _key, T _val){
    if(root == nullptr){
        root = new FaxTreeNode<int, T>(_key,_val);
        return root;
    }
    if(node == nullptr){
        return (new FaxTreeNode<int, T>(_key,_val));
    }
    if(node->key > _key){
        node->left = insertNode(node->left, _key, _val);
    }else if (node->key < _key){
        node->right = insertNode(node->right, _key, _val);
    }else if (node->key == _key){
        return node;
    }

    node->height = 1 + std::max(getHeight(node->left), getHeight(node->right));

    int balanceFactor = getBalance(node);

    if(balanceFactor > 1 && _key < node->left->key){
        return rotateRight(node);
    }
    if(balanceFactor < -1 && _key > node->right->key){
        return rotateLeft(node);
    }
    if(balanceFactor > 1 && _key > node->right->key){
        node->left = rotateLeft(node->left);
        return rotateRight(node);
    }
    if(balanceFactor < -1 && _key < node->left->key){
        node->right = rotateRight(node->right);
        return rotateLeft(node);
    }
    return node;

}

template <class T>
int FaxBTree<T>::getHeight(FaxTreeNode<int,T> *node){
    if(node == nullptr){
        return 0;
    }
    return std::max(getHeight(node->left), getHeight(node->right))+1;
}

template <class T>
int FaxBTree<T>::getBalance(FaxTreeNode<int, T> *node) {
    //return -1 if left heavy, 0 if balanced 1 if right heavy
    if (node == nullptr) {
        return 0;
    }
    return (getHeight(node->left) - getHeight(node->right));
}

template <class T>
FaxTreeNode<int,T>* FaxBTree<T>::rotateRight(FaxTreeNode<int,T> *x){

    FaxTreeNode<int,T> *y = x->left;
    FaxTreeNode<int,T> *t2 = y->right;

    if(x == this->root){
        this->root = y;
    }
    x->left = t2;
    y->right = x;

    x->height = (getHeight(x));
    y->height = (getHeight(y));

    return x;
}

template <class T>
FaxTreeNode<int,T>* FaxBTree<T>::rotateLeft(FaxTreeNode<int,T> *x){
    FaxTreeNode<int,T> *y = x->right;
    FaxTreeNode<int,T> *t2 = y->left;

    if(x == this->root){
        this->root = y;
    }
    y->left = x;
    x->right = t2;

    x->height = (getHeight(x));
    y->height = (getHeight(y));

    return y;
}



template <class T>
const void FaxBTree<T>::printTree(FaxTreeNode<int, T> *node, FaxQueue<FaxTreeNode<int,T>*> *printQueue){
    //if the tree is empty return nothing we didnt do anything
    if(this->root == nullptr){
        return;
    }
    //if the tree is not empty insert th root (node)
    if(printQueue == nullptr){
        printQueue = new FaxQueue<FaxTreeNode<int,T>*>();
        std::cout<<"\n";
    }
    printQueue->push(node);
    while(printQueue->head != nullptr){
        FaxTreeNode<int, T> *cursor = printQueue->tail->data;

        std::cout<< printQueue->tail->data->key<<"("<<printQueue->tail->data->height<<")" <<"\t";
        printQueue->pop();

        if(cursor->left != nullptr){
            printQueue->insertNode(cursor->left);
        }if(cursor->right != nullptr){
            printQueue->insertNode(cursor->right);
        }
    }

    delete(printQueue);
    std::cout<< "";
}

#endif //FAXLIB_FAXBTREE_H