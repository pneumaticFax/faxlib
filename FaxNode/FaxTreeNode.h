//
// Created by Jon_Austin on 2/24/2017.
//

#ifndef FAXLIB_FAXTREENODE_H
#define FAXLIB_FAXTREENODE_H


template <class key_T, class data_T>
class FaxTreeNode {

public:
    key_T key;
    data_T data;

    int height;

    FaxTreeNode *left;
    FaxTreeNode *right;

    FaxTreeNode():left(nullptr), right(nullptr), height(1){}
    FaxTreeNode(key_T _key, data_T _data):left(nullptr), right(nullptr), height(1), key(_key), data(_data){}
    FaxTreeNode(const FaxTreeNode& copy):left(copy.left), right(copy.right), height(copy.height), key(copy.key), data(copy.data){}
    ~FaxTreeNode(){delete (left); left = nullptr; delete (right); right = nullptr;}

    data_T getData(){return this->data;}
    key_T getKey(){return this->key;}
};

#endif //FAXLIB_FAXTREENODE_H
