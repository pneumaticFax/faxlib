//
// Created by Jon_Austin on 2/21/2017.
//

#ifndef FAXLIB_FAXNODE_H
#define FAXLIB_FAXNODE_H

template <class T>
class FaxNode {
public:
    FaxNode *prev;
    FaxNode *next;

    T data;

    FaxNode<T>();
    FaxNode<T>(T); //constructor with data initialized
    FaxNode<T>(const FaxNode<T> &);

    ~FaxNode<T>();

};

template <class T>
FaxNode<T>::FaxNode():prev(nullptr), next(nullptr), data(){}

template <class T>
FaxNode<T>::FaxNode(T data):prev(nullptr),next(nullptr), data(data){}

template <class T>
FaxNode<T>::FaxNode(const FaxNode<T> &copyNode):prev(copyNode.prev), next(copyNode.next), data(copyNode.data){}

template <class T>
FaxNode<T>::~FaxNode(){}



#endif //FAXLIB_FAXNODE_H
