#include <iostream>
#include <cstdio>
#include "FaxNode/FaxNode.h"
#include "FaxList/FaxList.h"
#include "FaxSet/FaxSet.h"
#include "FaxNode/FaxTreeNode.h"
#include "FaxBTree/FaxBTree.h"

int main() {
    FaxList<int> faxList;
    FaxNode<int> *faxNode =new FaxNode<int>(5);
    FaxList<int> *faxList2 = new FaxList<int>();


    FaxSet<int> *faxSet = new FaxSet<int>();
    FaxSet<int> *faxSet2 = new FaxSet<int>();
    FaxSet<int> *faxUnion = new FaxSet<int>();
    FaxSet<int> *faxIntersection;
    FaxSet<int> *faxDifference;


    FaxTreeNode<int, std::string> *faxKVNode = new FaxTreeNode<int, std::string>(1, "dog");


//    FaxTreeNode<std::string>;
    std::cout<<faxKVNode->getKey()<< faxKVNode->getData();

    FaxBTree<int> *faxTree = new FaxBTree<int>();
    faxTree->insertNode(faxTree->root, 5, 0);
    faxTree->printTree(faxTree->root, nullptr);

    faxTree->insertNode(faxTree->root, 4, 0);
    faxTree->printTree(faxTree->root, nullptr);

    faxTree->insertNode(faxTree->root, 3, 0);
    faxTree->printTree(faxTree->root, nullptr);

//    faxTree->rotateRight(faxTree->root);

    faxTree->insertNode(faxTree->root, 6, 0);
    faxTree->printTree(faxTree->root, nullptr);

    faxTree->insertNode(faxTree->root, 2, 0);
    std::cout<<"\n";
    faxTree->insertNode(faxTree->root, 1, 0);
    faxTree->insertNode(faxTree->root, 42, 0);
    faxTree->insertNode(faxTree->root, 35, 0);
    faxTree->insertNode(faxTree->root, 27, 0);

    faxTree->printTree(faxTree->root, nullptr);


    int height = faxTree->getHeight(faxTree->root);

    int balance = faxTree->getBalance(faxTree->root);



    delete(faxTree);

    return 0;
}