//
// Created by Jon_Austin on 2/24/2017.
//

#ifndef FAXLIB_FAXSLIST_H
#define FAXLIB_FAXSLIST_H

#include <iostream>
#include "../FaxNode/FaxNode.h"

template <class T>
class FaxSList {
public:
    FaxNode<T> *head, *tail;
    int size;

    FaxSList<T>();
    ~FaxSList<T>();

    FaxSList<T>* insertNode(T);            //insert node at the head
    FaxSList<T>* removeNode(FaxNode<T> *); // remove a node at location

    const int getSize(){return size;}
    const T getData(FaxNode<T> *node){return node->data;}
    const void print();
};

template <class T>
FaxSList<T>::FaxSList():head(nullptr), tail(nullptr), size(0) {}

template <class T>
FaxSList<T>::~FaxSList(){}

template <class T>
FaxSList<T>* FaxSList<T>::insertNode(T data) {
    FaxNode<T> *node = new FaxNode<T>(data);
    if(size == 0){
        head = node;
        tail = node;
        size++;
    }else{
        node->next = head;
        head = node;
        size++;
    }
    return this;
}

template <class T>
FaxSList<T>* FaxSList<T>::removeNode(FaxNode<T> *node){
    if(size == 1){
        head = nullptr;
        tail = nullptr;
    }
    if(node == head){
        head->next = head;
    }else{
        FaxNode<T> *cursor;
        for(cursor = head; cursor != nullptr; cursor = cursor->next){
            if(cursor->next == node){
                cursor->next = cursor->next->next;
                if (cursor->next == nullptr){
                    tail = cursor;
                }
            }
        }
    }
    delete(node);
    size--;
    return this;
}

template <class T>
const void FaxSList<T>::print(){

    FaxNode<T> *cursor;
    for(cursor = head; cursor != nullptr; cursor = cursor->next){
        std::cout<<cursor->data<<"\t\t";
    }std::cout<<"\n";
}

#endif //FAXLIB_FAXSLIST_H
