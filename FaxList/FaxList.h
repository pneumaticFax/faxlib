//
// Created by Jon_Austin on 2/21/2017.
//

#ifndef FAXLIB_FAXLIST_H
#define FAXLIB_FAXLIST_H

#include <iostream>
#include "../FaxNode/FaxNode.h"

template <class T>
class FaxList {

public:
    FaxList<T>();
    FaxList<T>(const FaxList<T> &);
    ~FaxList<T>();

    FaxList<T>* insertNodeNext(FaxNode<T> *, T);
    FaxList<T>* insertNodePrev(FaxNode<T> *, T);
    FaxList<T>* insertNodeHead(T);
    FaxList<T>* insertNodeTail(T);

    FaxList<T>* removeNodeNext(FaxNode<T> *);
    FaxList<T>* removeNodePrev(FaxNode<T> *);
    FaxList<T>* removeNodeHead();
    FaxList<T>* removeNodeTail();

    FaxList<T>* removeNode(FaxNode<T> *);

    FaxNode<T>* getHead(){return head;}
    FaxNode<T>* getTail(){return tail;}

    void printList();
    const void getData(FaxNode<T> *);
    bool setData(FaxNode<T>*, T);
    int getSize(){return size;}

private:
    FaxNode<T> *head, *tail;
    int size;

};



template<class T>
FaxList<T>::FaxList(): head(nullptr), tail(nullptr), size(0) {}


template<class T>
FaxList<T>::FaxList(const FaxList<T> &copyList):head(copyList.head), tail(copyList.tail), size(copyList.size) {}

template<class T>
FaxList<T>::~FaxList(){
    FaxNode<T> *cursor;
    while(head){
        cursor = head;
        head = head->next;
        delete cursor;
        size--;
    }
}

template<class T>
FaxList<T>* FaxList<T>::insertNodeNext(FaxNode<T> *element, T data){
    FaxNode<T> *nodeToInsert = new FaxNode<T>(data);
    if(size == 0 && element == nullptr){
        head = tail = nodeToInsert;
        nodeToInsert->prev = nodeToInsert->next= nullptr;
        size++;
        return this;
    }else if(element == head && element == tail){
        head->next = nodeToInsert;
        nodeToInsert->prev = head;
        tail = nodeToInsert;
        tail->prev = head;
        size++;
        return this;
    }else if(element == head){
        nodeToInsert->prev = head;
        nodeToInsert->next = head->next;
        head->next->prev = nodeToInsert;
        head->next = nodeToInsert;
        size++;
        return this;
    }else if(element == tail){
        tail->next = nodeToInsert;
        nodeToInsert->prev = tail;
        tail = nodeToInsert;
        size++;
        return this;
    }
    else {
        nodeToInsert->prev = element;
        nodeToInsert->next = element->next;
        element->next->prev = nodeToInsert;
        element->next = nodeToInsert;
        size++;
        return this;
    }
}

template<class T>
FaxList<T>* FaxList<T>::insertNodeTail(T data){
    return insertNodeNext(tail,  data);
}

template<class T>
FaxList<T>* FaxList<T>::insertNodePrev(FaxNode<T> *element, T data){

    FaxNode<T> *nodeToInsert = new FaxNode<T>(data);
    if(size == 0 && element == nullptr){
        head = tail = nodeToInsert;
        nodeToInsert->prev = nodeToInsert->next= nullptr;
        size++;
        return this;
    }else if(element == head && element == tail){
        tail->prev = nodeToInsert;
        nodeToInsert->next = tail;
        head = nodeToInsert;
        head->next = tail;
        size++;
        return this;
    }else if(element == head){
        nodeToInsert->next = head;
        nodeToInsert->prev = head->prev;
        head->prev = nodeToInsert;
        head = nodeToInsert;
        size++;
        return this;
    }else if(element == tail){
        //todo: verify this call
        head->prev = nodeToInsert;
        nodeToInsert->next = head;
        head = nodeToInsert;
        size++;
        return this;
    }
    else {
        nodeToInsert->next = element;
        nodeToInsert->prev = element->prev;
        element->prev->next = nodeToInsert;
        element->prev = nodeToInsert;
        size++;
        return this;
    }
}

template<class T>
FaxList<T>* FaxList<T>::insertNodeHead(T data){
    return insertNodePrev(head,  data);
}

template<class T>
FaxList<T>* FaxList<T>::removeNodeNext(FaxNode<T> *element){
    if(element == tail ){
        return this; // function does not remove anything after the tail
    }else if(element == head && size == 2){
        head->next = nullptr;
        tail = head;
        size--;
        return this;
    }else {
        element->next->next->prev = element;
        element->next = element->next->next;
        size--;
        return this;
    }
}


template<class T>
FaxList<T>* FaxList<T>::removeNodePrev(FaxNode<T> *element){
    if(element == head ){
        return this; // function does not remove anything after the head
    }else if(element == tail && size == 2){
        tail->prev = nullptr;
        head =tail;
        size--;
        return this;
    }else {
        element->prev->prev->next = element;
        element->prev = element->prev->prev;
        size--;
        return this;
    }
}


template<class T>
FaxList<T>* FaxList<T>::removeNodeHead(){
    if(size == 1) {
        head = nullptr;
        tail = nullptr;
        size--;
        return this;
    }else {
        head->next->prev = nullptr;
        head = head->next;
        size--;
        return this;
    }
}

template<class T>
FaxList<T>* FaxList<T>::removeNodeTail(){
    if(size == 1) {
        head = nullptr;
        tail = nullptr;
        size--;
        return this;
    }else {
        tail->prev->next = nullptr;
        tail = tail->prev;
        size--;
        return this;
    }
}
template<class T>
FaxList<T>* FaxList<T>::removeNode(FaxNode<T> *element){
}

template<class T>
const void FaxList<T>::getData(FaxNode<T> *element){
    return element->data;
}

template<class T>
bool FaxList<T>::setData(FaxNode<T> *element, T data){
    FaxNode<T> *cursor;
    for(cursor = head; cursor != nullptr; cursor = cursor->next){
        if(cursor == element){
            element->data = data;
            return true;
        }
    }
    return false;
}

template<class T>
void FaxList<T>::printList(){
    FaxNode<T> *cursor = this->head;
    while (cursor != nullptr){
        std::cout<<cursor->data<<"\t";
        cursor = cursor->next;
    }
    std::cout<<"\n";
}
#endif //FAXLIB_FAXLIST_H
